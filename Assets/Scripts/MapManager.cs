using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour 
{
    [HideInInspector] public static MapManager instance;

    [HideInInspector] public MapNode targetNode;
    [HideInInspector] public bool isTransitioning = false;

    [Space(5)]
    [SerializeField] public MapNode currentNode;
    [Space(5)]
    [SerializeField] float transitionTime = .2f;
    [Space(5)]
    [SerializeField] RectTransform cursor;

    float transitionTimer = 0;
    int direction;

    [Space(10)]
    [Tooltip("All the nodes, stops and invisibles included")]
        public MapNode[] nodes;

    /*[Space(5)]
    [Tooltip("All the nodes, stops and invisibles included")]
    public GameObject[] buttons;*/

    [Space(5)]
    [Tooltip("Menu")]
        public Menu menu;

    [Space(5)]
    [Tooltip("For debug only, unlock all levels")]
        [SerializeField] bool unlockAll = false;

    MapNode transitionNode;
    [HideInInspector] public List<MapNode> stops = new List<MapNode>();


    private void Awake() {
        if (instance == null) {
            instance = this;
        } else {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        for (int i = 0; i < nodes.Length; i++)
        {
            if (nodes[i].stop)
            {
                stops.Add(nodes[i]);
            }
        }
        for (int i = 0; i < SaveManager.saveData.unlockLevel + 1; i++)
        {
            stops[i].unlocked = true;
        }
        Debug.Log("Unlocked levels : " + SaveManager.saveData.unlockLevel);
        currentNode = nodes[0];

        if (unlockAll) //For debug
        {
            for (int i = 0; i < stops.Count; i++)
            {
                stops[i].unlocked = true;
            }
        }
    }

    private void Update()
    {
        if (targetNode!= null)
        {
            if (currentNode.index < targetNode.index)
            {
                direction = 1;
            }
            else
            {
                direction = -1;
            }
        }

        if (isTransitioning)
        {
            transitionNode = nodes[currentNode.index + direction];
            cursor.position = Vector3.Lerp(cursor.position, transitionNode.transform.position, transitionTimer / transitionTime);
            transitionTimer += Time.deltaTime;

            if (transitionTimer >= transitionTime || cursor.position == transitionNode.transform.position)
            {
                cursor.position = transitionNode.transform.position;
                transitionTimer = 0;
                if (cursor.position != targetNode.transform.position)
                {
                    currentNode = transitionNode;
                }
                else
                {
                    currentNode = targetNode;
                    targetNode = null;
                    isTransitioning = false;
                    menu.OpenAnyMenu(currentNode.gameObject);
                }
            }
        }
        else
        {
            if (currentNode.index != nodes.Length - 1 && (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.RightArrow) || Input.GetAxis("Horizontal") > 0 || Input.GetAxis("Horizontal") > 0)) 
            {
                for (int i = currentNode.index + 1; i < nodes.Length; i++)
                {
                    if (nodes[i].stop && nodes[i].unlocked)
                    {
                        targetNode = nodes[i];
                        isTransitioning = true;
                        menu.OpenAnyMenu(nodes[i].gameObject);
                        break;
                    }
                }
            }
            if (currentNode.index != 0 && (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.LeftArrow) || Input.GetAxis("Horizontal") < 0 || Input.GetAxis("Vertical") < 0))
            {
                for (int i = currentNode.index - 1; i < nodes.Length; i--)
                {
                    if (nodes[i].stop)
                    {
                        targetNode = nodes[i];
                        isTransitioning = true;
                        menu.OpenAnyMenu(nodes[i].gameObject);
                        break;
                    }
                }
            }
        }
    }
}
