using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fog : MonoBehaviour {
    [SerializeField] bool isVertical;
    [SerializeField] float speed = 1f, startOffset = 10f;

    private void Start() {
        if (GameManager.instance.checkPoints[SaveManager.saveData.currentCheckPoint] != null) {
            transform.position = GameManager.instance.checkPoints[SaveManager.saveData.currentCheckPoint].position + (isVertical ? Vector3.left : Vector3.down) * startOffset;
        }
    }

    private void Update() {
        transform.position += (isVertical ? Vector3.right : Vector3.up) * speed * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other) {
        if (other.TryGetComponent<Player>(out Player player)) {
            GameManager.instance.Death();
        }
    }
}
