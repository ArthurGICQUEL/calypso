﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;
    public static AudioManager instance;

    [HideInInspector]
    public float sfxVolume = -5;

    [HideInInspector]
    public bool muted = false;

    private AudioSource[] allAudioSources;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        //DontDestroyOnLoad(gameObject);

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
            s.source.outputAudioMixerGroup = s.mixerGroup;
        }
    }


    public void Play (string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound " + name + " not found.");
            return;
        }
        s.source.Play();

        //FindObjectOfType<AudioManager>().Play("");
    }
    public void Stop(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound " + name + " not found.");
            return;
        }
        s.source.Stop();

        //FindObjectOfType<AudioManager>().Stop("");
    }
    /*public void StopOrbitSounds()
    {
        string name1 = "Orbit1";
        Sound s1 = Array.Find(sounds, sound => sound.name == name1);
        if (s1 == null)
        {
            Debug.LogWarning("Sound " + name1 + " not found.");
            return;
        }
        s1.source.Stop();

        string name2 = "Orbit2";
        Sound s2 = Array.Find(sounds, sound => sound.name == name2);
        if (s2 == null)
        {
            Debug.LogWarning("Sound " + name2 + " not found.");
            return;
        }
        s2.source.Stop();

        string name3 = "Orbit3";
        Sound s3 = Array.Find(sounds, sound => sound.name == name3);
        if (s3 == null)
        {
            Debug.LogWarning("Sound " + name3 + " not found.");
            return;
        }
        s3.source.Stop();
    }
    public void StopRushSounds()
    {
        string name1 = "Rush1";
        Sound s1 = Array.Find(sounds, sound => sound.name == name1);
        if (s1 == null)
        {
            Debug.LogWarning("Sound " + name1 + " not found.");
            return;
        }
        s1.source.Stop();

        string name2 = "Rush2";
        Sound s2 = Array.Find(sounds, sound => sound.name == name2);
        if (s2 == null)
        {
            Debug.LogWarning("Sound " + name2 + " not found.");
            return;
        }
        s2.source.Stop();

        string name3 = "Rush3";
        Sound s3 = Array.Find(sounds, sound => sound.name == name3);
        if (s3 == null)
        {
            Debug.LogWarning("Sound " + name3 + " not found.");
            return;
        }
        s3.source.Stop();
    }
    /*public void StopAlarms()
    {
        string name1 = "Alarm1";
        Sound s1 = Array.Find(sounds, sound => sound.name == name1);
        if (s1 == null)
        {
            Debug.LogWarning("Sound " + name1 + " not found.");
            return;
        }
        s1.source.Stop();

        string name2 = "Alarm2";
        Sound s2 = Array.Find(sounds, sound => sound.name == name2);
        if (s2 == null)
        {
            Debug.LogWarning("Sound " + name2 + " not found.");
            return;
        }
        s2.source.Stop();
    }*/

    public void StopAllAudio()
    {
        allAudioSources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        //allAudioSources = gameObject.GetComponent(typeof(AudioSource)) as AudioSource[];
        foreach (AudioSource audioS in allAudioSources)
        {
            audioS.Stop();
        }
    }
}
