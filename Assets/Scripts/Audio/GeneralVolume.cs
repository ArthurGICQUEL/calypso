using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralVolume : MonoBehaviour
{
    //public Sound[] sounds;
    public static GeneralVolume instance;

    [HideInInspector]
    public float generalVolume = -5;

    [HideInInspector]
    public bool muted = false;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        //DontDestroyOnLoad(gameObject);
    }
}
