﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderManager : MonoBehaviour
{

    public Slider generalSlider, musicSlider, sfxSlider;

    void Start()
    {
        generalSlider.value = GeneralVolume.instance.generalVolume;
        musicSlider.value = MusicManager.instance.musicVolume;
        sfxSlider.value = AudioManager.instance.sfxVolume;
    }
}
