using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crate : MonoBehaviour {
    public LayerMask collidables;
    public FixedJoint fixedJoint;

    [HideInInspector] public Crate up, down;
    [HideInInspector] public bool isLowest {
        get => down == null;
    }
    [HideInInspector] public BoxCollider col;

    [SerializeField] bool debug, autoGenerateFixedJoints;
    [SerializeField] float groundCheckoutRaycastLenght = 1f;

    private void Awake() {
        col = GetComponent<BoxCollider>();
    }

    private void Update() {
        if (down == null && IsGrounded() && GetComponent<FixedJoint>() == null) {
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
        if (debug) {
            Debug.Log(IsGrounded());
            Debug.DrawRay(transform.position + Vector3.right * col.bounds.extents.x, -Vector3.up * ((col.bounds.extents.y + 0.2f) * groundCheckoutRaycastLenght), Color.red);
            Debug.DrawRay(transform.position + Vector3.left * col.bounds.extents.x, -Vector3.up * ((col.bounds.extents.y + 0.2f) * groundCheckoutRaycastLenght), Color.blue);
        }
    }

    private void OnCollisionEnter(Collision collision) {
        if (autoGenerateFixedJoints) {
            if (collision.gameObject.TryGetComponent<Crate>(out Crate crate)) {
                if (this.transform.position.y < crate.transform.position.y - 1.9f && Mathf.Abs(this.transform.position.x - crate.transform.position.x) < 1.8f) {
                    this.up = crate;
                    crate.down = this;
                    crate.transform.position = new Vector3(transform.position.x, crate.transform.position.y, crate.transform.position.z);
                    if (up.GetComponent<FixedJoint>() == null) {
                        fixedJoint = up.gameObject.AddComponent<FixedJoint>();
                        fixedJoint.connectedBody = this.GetComponent<Rigidbody>();
                    }
                }
            }
        }
    }

    private void OnCollisionExit(Collision collision) {
        if (autoGenerateFixedJoints) {
            collision.gameObject.TryGetComponent<Crate>(out Crate crate);
            if (crate == up) up = null;
            if (crate == down) down = null;
        }
    }

    public bool IsGrounded() {
        return Physics.Raycast(transform.position + Vector3.right * col.bounds.extents.x, -Vector3.up, (col.bounds.extents.y + 0.2f) * groundCheckoutRaycastLenght) || Physics.Raycast(transform.position + Vector3.left * col.bounds.extents.x, -Vector3.up, (col.bounds.extents.y + 0.2f) * groundCheckoutRaycastLenght);
    }
}
