using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Curly : MonoBehaviour
{
    public float speed;
    [HideInInspector]public float saveSpeed;
    public Transform curly;
    public TextMeshProUGUI textPro;
    public bool stop;

    // Start is called before the first frame update
    void Start()
    {
        saveSpeed = speed;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        curly.Translate(speed * Time.deltaTime, 0, 0);
    }

    private void OnTriggerEnter(Collider other)
    {
       if (other.tag == "Stop")
        {
            if (other.GetComponent<Animator>())
            {
                //speed = 0;
                Animator animator = other.GetComponent<Animator>();
                animator.SetBool("Activate", true);
            }
        } 
    }
}
