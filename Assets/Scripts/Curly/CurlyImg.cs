using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CurlyImg : MonoBehaviour
{
    public Curly curly;
    public bool end;
    [TextArea (8,1)]public string text;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("A1") || Input.GetButton("A2"))
        {
            ChangeScene();
        }
    }

    public void ChangeText()
    {
        curly.textPro.text = text;
    }

    public void OnTriggerEnter(Collider other)
    {
        if (end)
        {
            curly.speed = 0;
            StartCoroutine( CoroChangeScene());
        }
        else
        {
            ChangeText();
        }
    }

    public IEnumerator CoroChangeScene()
    {
        yield return new WaitForSeconds(3.5f);
        ChangeScene();
    }

    public void ChangeScene()
    {
        SceneManager.LoadScene(1);
    }
}
