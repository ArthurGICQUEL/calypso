using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour {
    [SerializeField] int index = 0;

    private void OnTriggerEnter(Collider other) {
        if (other.TryGetComponent<Player>(out Player player)) {
            if (SaveManager.saveData.currentCheckPoint < index) {
                SaveManager.saveData.currentCheckPoint = index;
                SaveManager.SaveJson();
            }
        }
    }
}
