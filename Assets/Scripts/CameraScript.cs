using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] Vector3 offset;
    [SerializeField] float smoothFactor, fixedY;
    [SerializeField] bool fixPositionY;
    bool isFollowing = true;
    public Vector3 startPosition;


    public static CameraScript instance;
    public Transform hostageTransform;
    [SerializeField] float interpolate;

    private void Awake()
    {
        startPosition.x = target.position.x;
        transform.position = target.position + offset;
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Update() {
        if (isFollowing && target.position.x > startPosition.x) {
            Vector3 targetPosition = target.position + offset;
            if (fixPositionY) {
                targetPosition.y = fixedY;
            }
            Vector3 smoothPosition = Vector3.Lerp(transform.position, targetPosition, smoothFactor * Time.deltaTime);
            transform.position = smoothPosition;
        }
    }


    public void HotageRescue()
    {
        isFollowing = false;

        transform.position = Vector3.Lerp(transform.position, new Vector3(hostageTransform.position.x, hostageTransform.position.y, transform.position.z), interpolate);

        Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, 30, interpolate);
    }
}
