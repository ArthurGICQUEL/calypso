using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public static GameManager instance;

    public Player player;
    public Transform[] checkPoints;

    bool pause = false;

    [Header("Menus")]
    [Tooltip("Menu displayed during pause")]
        [SerializeField] private GameObject pauseMenu;
    [Tooltip("Menu displayed during pause")]
        [SerializeField] private GameObject deathMenu;

    private void Awake() {
        if (instance == null) {
            instance = this;
        } else {
            Destroy(gameObject);
        }
        SaveManager.LoadJson();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }
    }

    public void Pause()
    {
        if (!pause)
        {
            Time.timeScale = 0;
            pause = true;
            pauseMenu.SetActive(!pauseMenu.activeSelf);
            pauseMenu.GetComponent<Menu>().OpenDefaultMenu();
        }
        else
        {
            Time.timeScale = 1;
            pause = !pause;
            pauseMenu.SetActive(false);
        }
    }

    public void Death()
    {
        player.stopMove = true;
        deathMenu.SetActive(true);
        player.tabDeath[0].SetActive(false);
        player.tabDeath[1].SetActive(true);
        pauseMenu.GetComponent<Menu>().OpenDefaultMenu();
    }

    /*public void Respawn()
    {
        player.pause = false;
        deathMenu.SetActive(false);

    }*/
}