using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Menu : MonoBehaviour
{
    [Space(5)]
    [Header("Menu")]
    [Tooltip("Audio Mixer")]
        public AudioMixer audioMixer;

    [Tooltip("Volume's slider")]
    [Space(5)]
        public Slider generalSlider, musicSlider, sfxSlider;

    [Tooltip("Mute's toggle")]
    [Space(5)]
    public Toggle generalToggle, musicToggle, sfxToggle;

    [Tooltip("Informations used for controllers' navigation")]
    [Space(5)]
        public GameObject firstButton, settingsFirstButton, settingsClosedButton;

    protected virtual void Start()
    {
        SaveManager.LoadJson();
        GeneralVolume.instance.generalVolume = SaveManager.saveData.saveVolumeGeneral;
        MusicManager.instance.musicVolume = SaveManager.saveData.saveVolumeMusic;
        AudioManager.instance.sfxVolume = SaveManager.saveData.saveVolumeSound;

        audioMixer.SetFloat("generalVolume", Mathf.Log10(GeneralVolume.instance.generalVolume) * 20);
        audioMixer.SetFloat("musicVolume", Mathf.Log10(MusicManager.instance.musicVolume) * 20);
        audioMixer.SetFloat("sfxVolume", Mathf.Log10(AudioManager.instance.sfxVolume) * 20);

        generalSlider.value = GeneralVolume.instance.generalVolume;
        musicSlider.value = MusicManager.instance.musicVolume;
        sfxSlider.value = AudioManager.instance.sfxVolume;

        generalToggle.isOn = GeneralVolume.instance.muted;
        musicToggle.isOn = MusicManager.instance.muted;
        sfxToggle.isOn = AudioManager.instance.muted;
    }

    #region Sound
    public void SetGeneralVolume(float volume)
    {
        audioMixer.SetFloat("generalVolume", Mathf.Log10(volume) * 20);
        GeneralVolume.instance.generalVolume = Mathf.Log10(volume) * 20;
        SaveManager.saveData.saveVolumeGeneral = Mathf.Log10(volume) * 20;
    }
    public void SetMusicVolume(float volume)
    {
        audioMixer.SetFloat("musicVolume", Mathf.Log10(volume) * 20);
        MusicManager.instance.musicVolume = Mathf.Log10(volume) * 20;
        SaveManager.saveData.saveVolumeMusic = Mathf.Log10(volume) * 20;

    }
    public void SetSFXVolume(float volume)
    {
        audioMixer.SetFloat("sfxVolume", Mathf.Log10(volume) * 20);
        AudioManager.instance.sfxVolume = Mathf.Log10(volume) * 20;
        SaveManager.saveData.saveVolumeSound = Mathf.Log10(volume) * 20;
    }

    public void MuteGeneralVolume(bool isMuted)
    {
        GeneralVolume.instance.muted = !GeneralVolume.instance.muted;
        if (isMuted)
        {
            audioMixer.SetFloat("generalVolume", -80);
            SaveManager.saveData.saveVolumeGeneral = -80;
        }
        else
        {
            audioMixer.SetFloat("generalVolume", GeneralVolume.instance.generalVolume);
        }
    }
    public void MuteMusicVolume(bool isMuted)
    {
        MusicManager.instance.muted = !MusicManager.instance.muted;
        if (isMuted)
        {
            audioMixer.SetFloat("musicVolume", -80);
            SaveManager.saveData.saveVolumeMusic = -80;

        }
        else
        {
            audioMixer.SetFloat("musicVolume", MusicManager.instance.musicVolume);
        }
    }
    public void MuteSFXVolume(bool isMuted)
    {
        AudioManager.instance.muted = !AudioManager.instance.muted;
        if (isMuted)
        {
            audioMixer.SetFloat("sfxVolume", -80);
            SaveManager.saveData.saveVolumeSound = -80;
        }
        else
        {
            audioMixer.SetFloat("sfxVolume", AudioManager.instance.sfxVolume);
        }
    }
    #endregion

    public void LoadScene(int sceneIndex)
    {
        MusicManager.instance.StopAllAudio();

        switch (sceneIndex)
        {
            case 2:
                MusicManager.instance.Play("0-Beach");
                break;

            case 3:
                MusicManager.instance.Play("1-Undergrowth");
                break;

            case 4:
                MusicManager.instance.Play("2-Forest");
                break;

            case 5:
                MusicManager.instance.Play("3-Magical_Forest");
                break;

            case 6:
                MusicManager.instance.Play("5-Magical_Forest_Runner");
                break;
        }
        
        SceneManager.LoadScene(sceneIndex);
    }

    public void ReloadScene()
    {
        Scene actualScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(actualScene.buildIndex);
    }

    public void SaveData()
    {
        SaveManager.SaveJson();
    }

    #region Controller
    public void Deselect()
    {
        EventSystem.current.SetSelectedGameObject(null);
    }

    public void OpenAnyMenu(GameObject selectedButton)
    {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(selectedButton);
    }

    public void OpenDefaultMenu()
    {
        OpenAnyMenu(firstButton);
    }

    public void OpenSettingsMenu()
    {
        OpenAnyMenu(settingsFirstButton);
    }

    public void CloseSettingsMenu()
    {
        OpenAnyMenu(settingsClosedButton);
    }
    #endregion
}
