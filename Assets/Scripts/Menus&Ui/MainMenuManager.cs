using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.EventSystems;

public class MainMenuManager : Menu
{
    #region Declarations
    [Space(20)]
    [Header("MainMenu")]

    [Space(20)]
    [Tooltip("Button selected when the credits are opened")]
        [SerializeField] private GameObject creditsFirstButton;

    [Space(5)]
    [Tooltip("Button selected when the credits are closed")]
        [SerializeField] private GameObject creditsClosedButton;

    [Space(20)]
    [Tooltip("Button selected when the select level menu is opened")]
        [SerializeField] private GameObject selectLevelFirstButton;

    [Space(5)]
    [Tooltip("Button selected when the select level menu is closed")]
        [SerializeField] private GameObject selectLevelClosedButton;


    [Space(10)]
    [Header("Transition")]

    [Space(10)]
    [Tooltip("Cinemachine Virtual Brain")]
        [SerializeField] private GameObject brain;

    [Space(5)]
    [Tooltip("Start Cinemachine Virtual Camera")]
        [SerializeField] private GameObject startCam;

    [Space(5)]
    [Tooltip("Transition Cinemachine Virtual Camera")]
        [SerializeField] private GameObject transitionCam;

    [Space(5)]
    [Tooltip("Select Cinemachine Virtual Camera")]
        [SerializeField] private GameObject selectCam;


    [Space(10)]
    [Header("SelectLevelMenu")]

    [Space(10)]
    [Tooltip("Objects that should disapear for the SelectLevelMenu")]
        [SerializeField] private GameObject[] slmObjects;

    [Space(5)]
    [Tooltip("SelectLevelMenu")]
        [SerializeField] private GameObject selectLevelMenu;

    [Space(5)]
    [Tooltip("Play buttons in the SelectLevelMenu")]
        public GameObject[] playButtons;

    [Space(5)]
    [Tooltip("Play buttons in the SelectLevelMenu")]
        public GameObject backSelectLevelButtons;

    [Space(5)]
    [Tooltip("Coul animator")]
        [SerializeField] Animator animator;

    [HideInInspector] public int unlockLevel;
    [HideInInspector] public bool animUnlockLevel = false;
    #endregion

    protected override void Start()
    {
        base.Start();
        startCam.SetActive(false);
        //MusicManager.instance

        for (int i = 0; i < playButtons.Length; i++)
        {
            playButtons[i].SetActive(false);
        }

        unlockLevel = SaveManager.saveData.unlockLevel;

        if (IntroSkipping.instance.menuloaded)
        {
            ActiveSelectLevelMenu(true, selectCam);
            EventSystem.current.SetSelectedGameObject(null);
            MusicManager.instance.StopAllAudio();
            MusicManager.instance.Play("Map");
            animUnlockLevel = SaveManager.saveData.animUnlockLevel;
            animator.SetBool("animUnlockLevel", animUnlockLevel);
            animator.SetInteger("unlockLevel", unlockLevel);
        }
        else
        {
            IntroSkipping.instance.menuloaded = true;
            MusicManager.instance.Play("MainMenu");
        }

        /*Debug.Log("List count : " + MapManager.instance.stops.Count + " & currentNode : " + MapManager.instance.currentNode);
        playButtons[System.Array.IndexOf(MapManager.instance.stops.ToArray(), MapManager.instance.currentNode)].SetActive(true);*/
    }

    #region Transition
    public void StartTransition()
    {
        StartCoroutine(Transition(transitionCam, selectCam, true));
    }

    public void RevertTransition()
    {
        StartCoroutine(Transition(selectCam, transitionCam, false));
    }

    IEnumerator Transition(GameObject vCam1, GameObject vCam2, bool state)
    {
        vCam1.SetActive(state);
        yield return new WaitForSeconds(2f);
        ActiveSelectLevelMenu(state, vCam2);
    }

    private void ActiveSelectLevelMenu(bool state, GameObject cam)
    {
        cam.SetActive(state);
        selectLevelMenu.SetActive(state);
        for (int i = 0; i < slmObjects.Length; i++)
        {
            slmObjects[i].SetActive(!state);
        }

        if (state)
        {
            OpenSelectLevelMenu();
            MusicManager.instance.Play("Map");
            animUnlockLevel = SaveManager.saveData.animUnlockLevel;
            animator.SetBool("animUnlockLevel", animUnlockLevel);
            animator.SetInteger("unlockLevel", unlockLevel);
        }
        else
        {
            CloseSelectLevelMenu();
            MusicManager.instance.Play("MainMenu");
        }
    }
    #endregion

    private void Update()
    {
        if (MapManager.instance != null)
        {
            if (MapManager.instance.isTransitioning)
            {
                for (int i = 0; i < playButtons.Length; i++)
                {
                    playButtons[i].SetActive(false);
                }
            }
            else
            {
                StartCoroutine(ShowPlayButton(0.05f));
            }
        }
    }

    IEnumerator ShowPlayButton(float timer)
    {
        yield return new WaitForSeconds(timer);
        if (MapManager.instance.stops.Contains(MapManager.instance.currentNode))
        {
            playButtons[System.Array.IndexOf(MapManager.instance.stops.ToArray(), MapManager.instance.currentNode)].SetActive(true);
        }
    }

    public void OpenSelectLevelMenu()
    {
        OpenAnyMenu(selectLevelFirstButton);
    }

    public void CloseSelectLevelMenu()
    {
        OpenAnyMenu(selectLevelClosedButton);
    }

    public void OpenCreditsMenu()
    {
        OpenAnyMenu(creditsFirstButton);
    }

    public void CloseCreditsMenu()
    {
        OpenAnyMenu(creditsClosedButton);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
