using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroSkipping : MonoBehaviour
{
    public static IntroSkipping instance;

    public bool menuloaded = false;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
    }
}
