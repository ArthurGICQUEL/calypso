using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : Menu
{
    public void Pause()
    {
        if (GameManager.instance != null) //This If/else loop is only used for debug. Must be replaced by only "GameManager.instance.Pause();" before the release.
        {
            GameManager.instance.Pause();
        }
        else
        {
            Debug.LogError("There's no GameManager in this scene");
        }
    }
}
