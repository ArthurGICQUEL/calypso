using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Tuto : MonoBehaviour
{
    [Tooltip("Sprites of text boxes")]
        [SerializeField] Sprite boxCaptain, boxSailor;

    [Tooltip("Sprites of icons")]
        [SerializeField] Sprite iconBottom, iconTop;

    [Tooltip("Sprite renderer of the text box")]
        [SerializeField] Image textBox;

    [Tooltip("Sprite renderer of the icon")]
        [SerializeField] Image icon;

    /*[Tooltip("")]
    [SerializeField] GameObject */

    [Tooltip("Text field")]
        [SerializeField] TextMeshProUGUI textField;

    [Tooltip("Texts of characters")]
        [SerializeField] string[] textsCaptain, textsSailor;

    [Tooltip("Time in seconds before the text disapear")]
        [SerializeField] float timer;

    bool defaultPlayer = true, captainSpeaking;
    Player player;

    private void Start()
    {
        player = GameManager.instance.player;
        //defaultPlayer = player.
    }

    public void DisplayText(int index, bool up)
    {
        StopCoroutine("Timer");
        icon.gameObject.SetActive(true);
        textBox.gameObject.SetActive(true);

        if (up)
        {
            icon.sprite = iconTop;

            if (defaultPlayer)
            {
                captainSpeaking = false;
            }
            else
            {
                captainSpeaking = true;
            }
        }
        else
        {
            icon.sprite = iconBottom;

            if (defaultPlayer)
            {
                captainSpeaking = true;
            }
            else
            {
                captainSpeaking = false;
            }
        }

        if (captainSpeaking)
        {
            textBox.sprite = boxCaptain;
            textField.text = textsCaptain[index];
        }
        else
        {
            textBox.sprite = boxSailor;
            textField.text = textsSailor[index];
        }

        StartCoroutine(Timer(timer));
    }

    IEnumerator Timer(float time)
    {
        yield return new WaitForSeconds(time);
        icon.gameObject.SetActive(false);
        textBox.gameObject.SetActive(false);
    }
}
