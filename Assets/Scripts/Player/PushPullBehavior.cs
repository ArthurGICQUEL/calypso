using System.Collections.Generic;
using UnityEngine;

public class PushPullBehavior : MonoBehaviour {
    [HideInInspector] public bool isPushing = false;

    [SerializeField] float distance = 1f, minDistance = .6f, speed = 5f, dirtyFloat = 1f;
    [SerializeField] LayerMask layerMask;

    float previousSpeed = 0f;
    Crate crate;
    Crate lastCrate;
    FixedJoint fixedJoint;

    Player player;

    private void Start() {
        player = GameManager.instance.player;
        previousSpeed = player.speed;
    }

    private void Update() {
        RaycastHit hit;

        Debug.DrawRay(transform.position + Vector3.up * .1f, Vector3.left * minDistance, Color.magenta, .1f);
        Debug.DrawRay(transform.position, Vector3.left * distance, Color.green, .1f);


        if (fixedJoint == null) isPushing = false;


        if (isPushing) {
            Collider crateCol = lastCrate.GetComponent<Collider>();
            if (Mathf.Abs(lastCrate.transform.position.x - transform.position.x) - crateCol.bounds.extents.x < minDistance) {
                if (lastCrate.transform.position.x - transform.position.x < 0f) {
                    lastCrate.transform.position -= Vector3.right * 0.1f;
                } else {
                    lastCrate.transform.position += Vector3.right * 0.1f;
                }
            }

            if (Mathf.Abs(lastCrate.transform.position.x - transform.position.x) - crateCol.bounds.extents.x > distance - .1f) {
                if (lastCrate.transform.position.x - transform.position.x < 0f) {
                    lastCrate.transform.position += Vector3.right * 0.1f;
                } else {
                    lastCrate.transform.position -= Vector3.right * 0.1f;
                }
            }
        }

        if ((Physics.Raycast(transform.position, Vector3.right, out hit, distance, layerMask)
        || Physics.Raycast(transform.position, Vector3.left, out hit, distance, layerMask))
        && (GameManager.instance.player.inverseControls ? Input.GetButtonDown("Y2") : Input.GetButtonDown("Y1"))
        && GetComponent<CharacterController>().isGrounded
        && !(Input.GetButtonDown("A1") || Input.GetButtonDown("A2"))) {
            if (Physics.Raycast(transform.position, Vector3.right, out RaycastHit hitRight, distance, layerMask)
            && Physics.Raycast(transform.position, Vector3.left, out RaycastHit hitLeft, distance, layerMask)) {
                if (lastCrate == hitRight.transform.GetComponent<Crate>()) hit = hitRight;
                else if (lastCrate == hitLeft.transform.GetComponent<Crate>()) hit = hitLeft;
                else hit = player.movement.x > 0 ? hitRight : hitLeft;
            }

            player.speed = this.speed;
            crate = hit.transform.gameObject.GetComponent<Crate>();
            Collider crateCol = crate.GetComponent<Collider>();
            if (Mathf.Abs(crate.transform.position.x - transform.position.x) - crateCol.bounds.extents.x < minDistance) {
                if (crate.transform.position.x - transform.position.x < 0f) {
                    crate.transform.position -= Vector3.right * 0.1f;
                } else {
                    crate.transform.position += Vector3.right * 0.1f;
                }
            }
            if (Mathf.Abs(crate.transform.position.x - transform.position.x) - crateCol.bounds.extents.x > distance - .1f) {
                if (crate.transform.position.x - transform.position.x < 0f) {
                    crate.transform.position += Vector3.right * 0.1f;
                } else {
                    crate.transform.position -= Vector3.right * 0.1f;
                }
            }

            if (fixedJoint != null) Destroy(fixedJoint);
            fixedJoint = crate.transform.gameObject.AddComponent<FixedJoint>();
            fixedJoint.connectedBody = GetComponentInChildren<Rigidbody>();
            fixedJoint.breakForce = 100000000000;
            fixedJoint.enableCollision = true;

            if (crate.down != null) Destroy(crate.down.fixedJoint);

            isPushing = true;
            player.animator.SetBool("Crate", true);
            StartCoroutine(player.AnimDelay("CrateLocker", true));

            if (crate.transform.position.x < transform.position.x)
            {
                player.animator.SetBool("CrateRight", true);
                player.spriteHolder.localScale = new Vector3(Mathf.Abs(player.spriteHolder.localScale.x) * 1, player.spriteHolder.localScale.y, player.spriteHolder.localScale.z);
            }
            else
            {
                player.animator.SetBool("CrateRight", false);
                player.spriteHolder.localScale = new Vector3(Mathf.Abs(player.spriteHolder.localScale.x) * -1, player.spriteHolder.localScale.y, player.spriteHolder.localScale.z);
            }
        }

        if (!GetComponent<CharacterController>().isGrounded || (lastCrate!= null && !lastCrate.IsGrounded()) || (crate != null && !crate.IsGrounded()) 
            || (GameManager.instance.player.inverseControls ? Input.GetButtonUp("Y2") : Input.GetButtonUp("Y1")) || Input.GetButtonDown("A1") || Input.GetButtonDown("A2")) {
            if (previousSpeed != 0) player.speed = previousSpeed;
            if (lastCrate != null && lastCrate.down != null && lastCrate.GetComponent<FixedJoint>() == null) {
                lastCrate.gameObject.AddComponent<FixedJoint>().connectedBody = lastCrate.down.GetComponent<Rigidbody>();
            }
            lastCrate = null;
            Destroy(fixedJoint);

            isPushing = false;
            player.animator.SetBool("Crate", false);
            player.animator.SetBool("CrateLocker", false);
        }

        if (crate != null) {
            lastCrate = crate;
            crate = null; 
        }
    }
}