using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public CharacterController charaController;
    public Animator animator;

    [Tooltip("Child of the character with the sprite library")]
        public Transform spriteHolder;

    public bool pause = false;
    public bool stopMove = false;

    // Movement
    [HideInInspector]public Vector3 movement;
    public float speed;
    public float dashStrengh;
    //float dashCD;
    //bool dash = false;
    bool sneaky = false;

    // Jump
    public bool isJumping;
    public float jumpHeight;
    public float timingJumpMax;
    float timing = 0;
    float gravity = 30;
    bool doubleJump;
    bool hoverIsPressed = false;

    PushPullBehavior crateScript;
    [HideInInspector] public bool inverseControls = false;
    float timer = 0f;
    [SerializeField] const float inverseControlTime = 30f;
    [SerializeField] Animator switchAnim;
    public GameObject[] tabDeath;


    void Awake()
    {
        charaController = GetComponent<CharacterController>();

        if (GameManager.instance.checkPoints.Length > 0 && GameManager.instance.checkPoints[SaveManager.saveData.currentCheckPoint] != null) {
            transform.position = GameManager.instance.checkPoints[SaveManager.saveData.currentCheckPoint].position;
        }
    }

    private void Start()
    {
        crateScript = gameObject.GetComponent<PushPullBehavior>();

        spriteHolder.localScale = new Vector3(Mathf.Abs(spriteHolder.localScale.x) * -1, spriteHolder.localScale.y, spriteHolder.localScale.z);
    }

    void Update()
    {
        //dashCD -= Time.deltaTime;
        //Debug.DrawRay(transform.position, base.transform.up * 2f, Color.red);

        timer += Time.deltaTime;
        if (timer >= inverseControlTime) {
            StartCoroutine("SwitchDelay");
            switchAnim.SetTrigger("tourbilol");
            timer = 0f;
        }

        RaycastSneaky();

        Movement();

        if (Time.timeSinceLevelLoad < .05f) {
            if (GameManager.instance.checkPoints.Length > 0 && GameManager.instance.checkPoints[SaveManager.saveData.currentCheckPoint] != null) {
                transform.position = GameManager.instance.checkPoints[SaveManager.saveData.currentCheckPoint].position;
            }
        }
    }

    void Movement() {
        if (stopMove) {
            return;
        }

        #region Movement
        float inputX = Input.GetAxis(inverseControls ? "Horizontal" : "Horizontal2");
        movement.x = inputX * speed;
        animator.SetFloat("Speed", movement.x);
        #endregion

        #region Flip
        if (!crateScript.isPushing)
        {
            if (inputX > 0)
            {
                spriteHolder.localScale = new Vector3(Mathf.Abs(spriteHolder.localScale.x) * -1, spriteHolder.localScale.y, spriteHolder.localScale.z);
            }
            else if (inputX < 0)
            {
                spriteHolder.localScale = new Vector3(Mathf.Abs(spriteHolder.localScale.x) * 1, spriteHolder.localScale.y, spriteHolder.localScale.z);
            }
        }
        #endregion

        if (Input.GetButtonUp(inverseControls ? "A2" : "A1")) {
            doubleJump = true;
        }


        #region Jump&Ground
        if (charaController.isGrounded) {

            movement.y = -1;
            animator.SetBool("Jump1", false);
            animator.SetBool("Jump2", false);
            animator.SetBool("DoubleJump", false);
            animator.SetBool("Hover", false);
            hoverIsPressed = false;

            doubleJump = false;

            // Jump J1
            if (Input.GetButtonDown(inverseControls ? "A1" : "A2") && /*!dash &&*/ RaycastSneaky()) {
                timing = 0;
                animator.SetBool("Jump1", true);
                isJumping = true;
            }

            //  Jump J2
            if (Input.GetButtonDown(inverseControls ? "A2" : "A1") && /*!dash &&*/ RaycastSneaky()) {
                doubleJump = true;
                timing = 0;
                animator.SetBool("Jump2", true);
                isJumping = true;
            }

            // Dash
            /*if (dashCD <= 0) {
                dash = false;
                if (Input.GetButtonDown("Dash")) {
                    StartCoroutine(DashCoroutine());
                }
            }*/

            // Sneaky
            if (Input.GetButtonDown(inverseControls ? "B2" : "B1")) {
                sneaky = true;
                SneakyFunct();
            }

            if (Input.GetButtonUp(inverseControls ? "B2" : "B1") && RaycastSneaky()) {
                sneaky = false;
                SneakyFunct();
            }

            if (!Input.GetButton(inverseControls ? "B2" : "B1") && sneaky && RaycastSneaky()) {
                sneaky = false;
                SneakyFunct();
            }

            /*if (Input.GetKeyDown(KeyCode.Mouse0)) {
                StartCoroutine(DashCoroutine());
            }*/
        } else {
            // In air
            if (Input.GetButtonDown(inverseControls ? "B2" : "B1")) {
                hoverIsPressed = true;
            }

            if (Input.GetButtonUp(inverseControls ? "B2" : "B1")) {
                hoverIsPressed = false;
            }

            if (!RaycastSneaky())
            {
                movement.y -= gravity * Time.deltaTime;
            }

            if (!doubleJump && Input.GetButtonDown(inverseControls ? "A2" : "A1") && !hoverIsPressed && RaycastSneaky()) {
                movement.y = 0;
                movement.y += jumpHeight * 1.15f;
                doubleJump = true;
                animator.SetBool("DoubleJump", true);
                isJumping = false;
                StartCoroutine(AnimDelay("DoubleJump", false));
            }
            if (!hoverIsPressed || charaController.velocity.y > -3.5f) {
                movement.y -= gravity * Time.deltaTime;
            }
        }

        if (isJumping) {
            JumpFunct();
        }

        if (hoverIsPressed)
        {
            animator.SetBool("Hover", true);
            if (charaController.velocity.y <= -3.5f) {
                movement.y = -3.5f;
            }
        }
        else
        {
            animator.SetBool("Hover", false);
        }
        animator.SetBool("InAir", !charaController.isGrounded);
        #endregion
        charaController.Move(movement * Time.deltaTime);
        transform.position = new Vector3(transform.position.x, transform.position.y, 0f);
    }

    public IEnumerator AnimDelay(string parameterName, bool state)
    {
        yield return new WaitForSeconds(0.2f);
        animator.SetBool(parameterName, state);
    }

    public void JumpFunct()
    {
        timing += Time.deltaTime;

        movement.y = 1 + jumpHeight / 1.5f;

        if (Input.GetButtonUp("A1") || Input.GetButtonUp("A2") || timing >= timingJumpMax) {
            isJumping = false;
            return;
        }


        if (sneaky)
        {
            sneaky = false;
            SneakyFunct();
        }
    }

    public void SneakyFunct()
    {
        if (sneaky)
        {
            animator.SetBool("Sneaky", true);
            gameObject.GetComponent<Animator>().SetBool("Sneak", true);
        }
        if (!sneaky)
        {
            animator.SetBool("Sneaky", false);
            gameObject.GetComponent<Animator>().SetBool("Sneak", false);
        }
    }

    /*IEnumerator DashCoroutine()
    {
        dash = true;
        dashCD = 0.65f;
        float startTime = Time.time;
        while (Time.time < startTime + 0.5f)
        {
            if (movement.x > 0)
            {
                base.transform.Translate(base.transform.right * dashStrengh * Time.deltaTime);
                animator.SetBool("Dash", true);
            }

            if (movement.x < 0)
            {
                base.transform.Translate(-base.transform.right * dashStrengh * Time.deltaTime);
                animator.SetBool("Dash", true);
            }
            yield return null;
            animator.SetBool("Dash", false);
        }
    */

    bool RaycastSneaky()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, base.transform.up * 2f, out hit, 2f))
        {
            return false;
        }
        return true;
    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {

        if (hit.gameObject.GetComponent<TutoHolder>())
        {
            TutoHolder tutoHolder = hit.gameObject.GetComponent<TutoHolder>();
            gameObject.GetComponent<Tuto>().DisplayText(tutoHolder.index, tutoHolder.up);
            hit.gameObject.SetActive(false);
        }
    }

    IEnumerator SwitchDelay()
    {
        yield return new WaitForSeconds(2f);
        inverseControls = !inverseControls;
    }
}
