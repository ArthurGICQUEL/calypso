using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapNode : MonoBehaviour
{
    [HideInInspector] public MapNode previousNode, nextNode;
    [HideInInspector] public int index;
    public bool stop;
    public bool unlocked = false;
    MapManager mapManager;

    private void Start()
    {
        mapManager = MapManager.instance;
        index = System.Array.IndexOf(mapManager.nodes, this);
        if (index == 0)
        {
            unlocked = true;
        }

        if (index != 0)
        {
            previousNode = mapManager.nodes[index - 1];
        }
        if (index >= mapManager.nodes.Length)
        {
            nextNode = mapManager.nodes[index + 1];
        }
    }

    public void GoToThisNode()
    {
        if (mapManager.currentNode == this)
        {
            mapManager.menu.LoadScene(System.Array.IndexOf(mapManager.stops.ToArray(), this) + 2);
        }
        else if (unlocked)
        {
            MapManager.instance.targetNode = this;
            MapManager.instance.isTransitioning = true;
        }
    }
}
