using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class SaveManager : MonoBehaviour
{
    public static SaveManager instance;
    [HideInInspector] public static SaveData saveData = new SaveData();
    [HideInInspector] public static string filePath; //= Application.persistentDataPath + "/SaveData.json";
    private static string key = "kob576yre076cdr64VGHI78O9";

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
        filePath = Application.persistentDataPath + "/SaveData.json";

        //Debug.Log("Chemin : " + filePath);
    }

    void Update()
    {
        //Debug.Log("Animunlock : " + saveData.animUnlockLevel);
    }

    static string Crypted(string data)
    {
        string crypted = "";
        for (int i = 0; i < data.Length; i++)
        {
            crypted = crypted + (char)(data[i] ^ key[i % key.Length]);
        }
        return crypted;
    }

    static string Decrypted(string data)
    {
        string decrypted = "";
        for (int i = 0; i < data.Length; i++)
        {
            decrypted = decrypted + (char)(data[i] ^ key[i % key.Length]);
        }
        return decrypted;
    }

    public static void SaveJson()
    {
        string dataString = JsonUtility.ToJson(saveData);
        string crypted = Crypted(dataString);
        System.IO.File.WriteAllText(Application.persistentDataPath + "/SaveData.json", crypted);
    }

    public static void LoadJson()
    {
        if (File.Exists(Application.persistentDataPath + "/SaveData.json"))
        {
            string dataString = System.IO.File.ReadAllText(Application.persistentDataPath + "/SaveData.json");

            string decrypted = Decrypted(dataString);

            saveData = JsonUtility.FromJson<SaveData>(decrypted);
        }
        else
        {
            return;
        }
    }


}
