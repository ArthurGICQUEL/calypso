using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData
{
    public int currentCheckPoint;

    public int unlockLevel = 0;
    public float saveVolumeGeneral, saveVolumeSound, saveVolumeMusic;
    public bool animUnlockLevel = false;

    public SaveData() {

    }
}
