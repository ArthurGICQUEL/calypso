﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxInfinite : MonoBehaviour
{
    float length, startPosX, startPosY;
    public GameObject cam;
    public float parallaxEffect;
    public bool infinite;
    

    // Start is called before the first frame update
    void Start()
    {
        startPosX = transform.position.x;
        startPosY = transform.position.y;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
        
    }

    private void FixedUpdate()
    {
        //translate from cam position
        float temp = cam.transform.position.x * (1 - parallaxEffect);
        float dist = cam.transform.position.x * parallaxEffect;
        transform.position = new Vector3(startPosX + dist, startPosY, transform.position.z);

        //infinite repetition : translation of one screen
        if (infinite)
        {
            if (temp > startPosX + length)
            {
                startPosX += length;
            }
            else if (temp < startPosX - length)
            {
                startPosX -= length;
            }
        }

        // Block the camera at certain position
        //else
        //{
        //    if (cam.transform.position.x > 14.8f || cam.transform.position.x < -14.8f)
        //    {
        //        move = false;
        //    }
        //    else
        //    {
        //        move = true;
        //    }
        //}
    }
}
