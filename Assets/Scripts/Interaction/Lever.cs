using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lever : MonoBehaviour
{
    public Animator[] animator;
    public float offset;
    bool openDoor = false;
    bool inRange = false;
    public Player player;
    bool isActivate = false;

    private void Update()
    {
        if (Input.GetButtonDown(GameManager.instance.player.inverseControls ? "X1" : "X2") && inRange && !isActivate && player.charaController.isGrounded)
        {
            Debug.Log("Appuie");
            ActivateLever();
            isActivate = true;
            player.stopMove = true;
            player.gameObject.transform.position = new Vector3(transform.position.x + offset, player.transform.position.y, player.transform.position.z);

            player.animator.SetBool("Activation1", true);
            StartCoroutine(player.AnimDelay("Activation1", false));
        }
    }

    public void ActivateLever() {
        if (!openDoor)
        {
            animator[0].SetBool("Activate", true);
            openDoor = true;
        }
    }

    public void MoovePlayer()
    {
        player.stopMove = false;
    }

    public void OpenDoor()
    {
        if (openDoor)
        {
            for (int i = 1; i < animator.Length; i++)
            {
                animator[i].SetBool("Activate", true);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            inRange = true;
            Debug.Log("Entre : " + inRange);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            inRange = false;
            Debug.Log("Sort : " + inRange);
        }
    }
}
