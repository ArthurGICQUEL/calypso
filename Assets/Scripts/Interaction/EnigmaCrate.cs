using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnigmaCrate : MonoBehaviour
{
    [SerializeField] Transform initPosition;
    [SerializeField] bool unlock;
    [SerializeField] Animator[] animator;
    [SerializeField] Crate crate;
    Vector3 sizeCollider;
    bool firstActivate = false;

    void Start()
    {

    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Crate")
        {
            sizeCollider = crate.col.size;
            if (unlock)
            {
                for (int i = 0; i < animator.Length; i++)
                {
                    animator[i].SetBool("Activate", true);
                }
            }
            else
            {
                if (!firstActivate)
                {
                    firstActivate = true;
                    animator[0].SetBool("Activate", true);
                    StartCoroutine(SizeCollider());
                }
                else
                {
                    crate.col.size = new Vector3(1f, 1f, 1f);
                    animator[1].SetBool("Activate", true);
                }

            }
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if(other.tag == "Crate")
        {
            crate.col.size = sizeCollider;
            crate.transform.position = initPosition.position;
        }
    }

    IEnumerator SizeCollider()
    {
        yield return new WaitForSeconds(0.75f);
        crate.col.size = new Vector3(1f, 1f, 1f);
        animator[1].SetBool("Activate", true);
        //crate.col.enabled = false;
    }

    public void DesactivateStele()
    {
        animator[1].SetBool("Activate", false);
    }
}
