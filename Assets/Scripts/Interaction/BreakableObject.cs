using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableObject : MonoBehaviour
{
    Animator animator;
    RaycastHit hit;
    //public float raycastRange;
    public Vector3 raycastStartPosition;
    public Vector3 raycastDirection;

    [SerializeField] Player player;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        player = GameManager.instance.player;
    }

    private void Update()
    {
        Debug.DrawRay(transform.position - raycastStartPosition, raycastDirection, Color.red);

        if (!RaycastCollision())
        {
            //Debug.Log("Collision : " + hit.transform.gameObject);

            if (hit.transform.tag == "Player")
            {
                Debug.Log("Collision with Player");
                if (Input.GetButtonDown(player.inverseControls ? "B1" : "B2"))
                {
                    player.animator.SetBool("Breaking", true);
                    StartCoroutine(player.AnimDelay("Breaking", false));
                    BreakObject();
                }
            }
        }
    }

    public void BreakObject()
    {
        animator.SetBool("Break", true);
    }

    bool RaycastCollision()
    {
        if (Physics.Raycast(transform.position - raycastStartPosition, raycastDirection, out hit, 4f))
        {
            return false;
        }
        return true;

    }
}
