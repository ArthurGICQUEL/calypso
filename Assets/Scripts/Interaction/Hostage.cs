using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Hostage : MonoBehaviour
{
    Animator animator;
    public int numberNextLevel;
    [SerializeField]AudioClip clipAudio;
    AudioSource audioSource;
    public GameObject fadeIn;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ChangeAudio()
    {
        audioSource.clip = clipAudio;
        audioSource.Play();
    }

    private void Fade()
    {
        Debug.Log("Fade");
        fadeIn.SetActive(true);
        StartCoroutine(GoMenu());
    }

    private  void OnTriggerStay(Collider other)
    {
        if(other.tag == "Player")
        {
            Player player = other.GetComponent<Player>();
            animator.SetBool("Saved", true);
            if (player.charaController.isGrounded)
            {
                player.animator.SetBool("InAir", false);
                player.animator.SetBool("Jump1", false);
                player.animator.SetBool("Hover", false);
                //Debug.Log("Grounded");
                player.animator.SetFloat("Speed", 0);
                player.stopMove = true;
                //player.charaController.detectCollisions = false;
                //player.charaController.enabled = false;
            }

            CameraScript.instance.HotageRescue();


            if (SaveManager.saveData.unlockLevel < numberNextLevel)
            {
                Debug.Log("Rentre dans la condition");
                SaveManager.saveData.unlockLevel = numberNextLevel;
                SaveManager.saveData.animUnlockLevel = true;
                SaveManager.SaveJson();
            }
        }
    }

    public IEnumerator GoMenu()
    {
        yield return new WaitForSeconds(3.5f);
        SceneManager.LoadScene(1);
    }
}
