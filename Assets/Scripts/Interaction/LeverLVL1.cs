using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverLVL1 : MonoBehaviour
{
    public Animator[] animator;
    bool openDoor = false;
    bool inRange = false;
    public Player player;
    bool isActivate = false;

    // Level 1
    public LeverLVL1[] tabLever;
    public bool level1;
    public int nbrLever;
    static int leverNeedActiv = 3;
    static int leverActiv = 0;

    public float offset = -1.5f;

    private void Update()
    {
        if (Input.GetButtonDown(GameManager.instance.player.inverseControls ? "X1" : "X2") && inRange && !isActivate && player.charaController.isGrounded)
        {
            animator[0].SetBool("Desactivate", false);
            //Debug.Log("Appuie / leverActiv : " + leverActiv);

            animator[0].SetBool("Activate", true);
            ActivateLever();
            player.stopMove = true;
            player.gameObject.transform.position = new Vector3(transform.position.x + offset, player.transform.position.y, player.transform.position.z);

            player.animator.SetBool("Activation1", true);
            StartCoroutine(player.AnimDelay("Activation1", false));
        }
        //Debug.Log("Moove : " + player.stopMove);
    }

    public void ActivateLever()
    {
        if (!openDoor && !isActivate)
        {
            isActivate = true;
            if (level1)
            {
                if (leverActiv == nbrLever)
                {
                    leverActiv++;
                    Debug.Log("Cas1 : " + leverActiv);
                    animator[1].SetBool("Activate", true);
                    if (leverActiv == leverNeedActiv)
                    {
                        openDoor = true;
                        Debug.Log("Cas3");
                        //player.stopMove = false;
                    }

                }
                else if (leverActiv != nbrLever)
                {
                    for (int i = 0; i < tabLever.Length; i++)
                    {
                        if (tabLever[i].isActivate)
                        {
                            tabLever[i].isActivate = false;
                            tabLever[i].animator[0].SetBool("Desactivate", true);
                            tabLever[i].animator[1].SetBool("Activate", false);
                            //tabLever[i].animator[0].SetBool("Activate", false);
                            Debug.Log("Cas2Tableau");
                        }
                    }
                    Debug.Log("Cas2");

                    //player.stopMove = false;
                    leverActiv = 0;
                }
            }
            else
            {
                openDoor = true;
                //player.stopMove = false;
            }
        }
    }

    public void MoovePlayer()
    {
        player.stopMove = false;
        animator[0].SetBool("Activate", false);
    }

    public void OpenDoor()
    {
        //Debug.Log("OpenDoor : " + openDoor);
        if (openDoor)
        {
            //Debug.LogError("IFOpenDoor");
            animator[1].SetBool("Activate", true);
            animator[2].SetBool("Activate", true);
            //if (animator.Length > 2)
            //{
            //    animator[2].SetBool("Activate", true);
            //}
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            inRange = true;
            //Debug.Log("Entre : " + inRange);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            inRange = false;
            //Debug.Log("Sort : " + inRange);
        }
    }
}
